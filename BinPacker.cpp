#include "BinPacker.h"

#include <iostream>


bool BinNode::Divide(SeparatorType separatorType, float amount)
{
	if(amount <= 0)
	{
		std::cout << "ERROR: amount is less than zero(" << amount << ")" << std::endl;
		return false;
	}
	
	// divide the node into two sub-nodes
	switch(separatorType)
	{
		case SeparatorType::None:
			break;
			
		case SeparatorType::Horizontal:
			if(amount > size.x)
			{
				std::cout << "ERROR: not enough space" << std::endl;
				return false;
			}
			
			type = SeparatorType::Horizontal;
			children = new BinNode[2];
			children[0].position = position;
			children[0].size.Set(amount, size.y);
			children[1].position = position + Vector2(amount, 0.0f);
			children[1].size.Set(size.x - amount, size.y);
			break;
			
		case SeparatorType::Vertical:
			if(amount > size.y)
			{
				std::cout << "ERROR: not enough space" << std::endl;
				return false;
			}
			
			type = SeparatorType::Vertical;
			children = new BinNode[2];
			children[0].parent = this;
			children[0].position = position;
			children[0].size.Set(size.x, amount);
			children[1].parent = this;
			children[1].position = position + Vector2(0.0f, amount);
			children[1].size.Set(size.x, size.y - amount);
			break;
	}
	return true;
}

bool BinNode::InsertRect(BinRect *rect, float padding)
{
	// don't process if rect is nullptr or if this node is already occupied
	if(!rect || occupied)
		return false;
	
	// attempt to insert into children, if available, or divide, and insert
	bool result = false;
	if(children)
	{
		result = children[0].InsertRect(rect, padding);
		if(!result)
			result = children[1].InsertRect(rect, padding);
	} else {
		// normal-hor, normal-vert, rotated-hor, rotated-vert
		float leftoverArea[4] = { -1.0f, -1.0f, -1.0f, -1.0f };
		Vector2 paddedSize(rect->size.x + padding, rect->size.y + padding);
		
		// determine if the rect fits without rotation
		if(paddedSize.x <= size.x && paddedSize.y <= size.y)
		{
			// find the left-over area if initially divided horizontally
			leftoverArea[0] = (size.x - paddedSize.x) * size.y; // normal orientation, divided horizontally
			leftoverArea[1] = size.x * (size.y - paddedSize.y); // normal orientation, divided vertically
		}
		
		// determine if the rect fits with rotation
		if(paddedSize.x <= size.y && paddedSize.y <= size.x)
		{
			// find the over-over area if initially divided vertically
			leftoverArea[2] = (size.x - paddedSize.y) * size.y; // rotated orientation, divided horizontally
			leftoverArea[3] = size.x * (size.y - paddedSize.x); // rotated orientation, divided vertically
		}
		
		// find the orientation/division with the highest left-over area
		int targetIndex = 0;
		float highestValue = leftoverArea[0];
		for(int i=0;i<3;++i)
		{
			if(leftoverArea[i+1] > highestValue)
			{
				highestValue = leftoverArea[i+1];
				targetIndex = i+1;
			}
		}
		
		// make sure the highest value is > 0.0f which guarantees a fit was found
		if(highestValue >= 0.0f)
		{
			// flip padded values if rotated
			if(targetIndex > 1)
			{
				rect->Rotate();
				float temp = paddedSize.x;
				paddedSize.x = paddedSize.y;
				paddedSize.y = temp;
			}
				
			// divide based on highest index
			switch(targetIndex)
			{
					// normal orientation, divided horizontally
				case 0:
					if(Divide(SeparatorType::Horizontal, paddedSize.x))
						children[0].Divide(SeparatorType::Vertical, paddedSize.y);
					break;
					
					// normal orientation, divided vertically
				case 1:
					if(Divide(SeparatorType::Vertical, paddedSize.y))
						children[0].Divide(SeparatorType::Horizontal, paddedSize.x);
					break;
					
					// rotated orientation, divided horizontally
				case 2:
					if(Divide(SeparatorType::Horizontal, paddedSize.x))
						children[0].Divide(SeparatorType::Vertical, paddedSize.y);
					break;
					
					// rotated orientation, divided vertically
				case 3:
					if(Divide(SeparatorType::Vertical, paddedSize.y))
						children[0].Divide(SeparatorType::Horizontal, paddedSize.x);
					break;
			}
			
			// set the rect's position and make it occupied
			rect->position = children[0].children[0].position + Vector2(padding, padding);
			children[0].children[0].occupied = true;
			result = true; // found
		}
	}
	return result;
}

int BinNode::GetNumNodes()
{
	int numNodes = 0;
	if(children)
		numNodes += children[0].GetNumNodes() + children[1].GetNumNodes();
	else
		++numNodes;
	return numNodes;
}
