#ifndef _BIN_PACKER_H_
#define _BIN_PACKER_H_

#include "Types.h"

#include <iostream>
#include <algorithm>
#include <vector>


enum class SeparatorType
{
	None,
	Horizontal,
	Vertical
};


class BinRect : public Rect
{
private:
	uint8_t rotated;

public:
	BinRect() : Rect(), rotated(0) { }
	BinRect(float width, float height) : Rect(width, height), rotated(0) { }
	
	void Rotate()
	{
		float temp = size.x;
		size.x = size.y;
		size.y = temp;
		rotated ^= 1;
	}
	
	virtual bool CanPack() = 0;
	bool IsRotated() { return rotated > 0; }
	float GetPaddedArea(float padding) { return (size.x + padding) + (size.y + padding); }
};


class BinNode : public Rect
{
private:
	uint8_t occupied;
	SeparatorType type;
	BinNode *parent;
	BinNode *children;
	
public:
	BinNode() : Rect(), occupied(0), type(SeparatorType::None), parent(nullptr), children(nullptr) { }
	BinNode(BinNode *parent) : Rect(), occupied(0), type(SeparatorType::None), parent(parent), children(nullptr) { }
	BinNode(float width, float height, BinNode *parent) :
	Rect(width, height), occupied(0), type(SeparatorType::None), parent(parent), children(nullptr) { }
	BinNode(float x, float y, float width, float height, BinNode *parent) :
	Rect(x, y, width, height), occupied(0), type(SeparatorType::None), parent(parent), children(nullptr) { }
	
	virtual ~BinNode()
	{
		// release the children
		if(children != nullptr)
		{
			delete [] children;
			children = nullptr;
		}
	}
	
	bool Divide(SeparatorType separatorType, float amount);
	bool InsertRect(BinRect *rect, float padding);
	
	bool IsOccupied() { return occupied ? true : false; }
	bool DoesRectFit(const Rect &rect) { return size.x >= rect.size.x && size.y >= rect.size.y; }
	int GetNumNodes();
	BinNode *GetParent() { return parent; }
};


template<class TBinRect>
bool CompareBinRects(TBinRect *a, TBinRect *b)
{
	if (a == nullptr || b == nullptr)
		return false;
	return a->GetArea() < b->GetArea();
}


template<class TBinRect>
class BinPacker
{
protected:
	bool forceSquare;
	float spaceWasted;
	int padding, atlasWidth, atlasHeight;
	BinNode *root;
	std::vector<TBinRect*> rects;

	virtual bool PreCommit() { return true; }
	virtual void PostCommit() { }
	
public:
	BinPacker() : forceSquare(false), spaceWasted(0.0f), padding(0), atlasWidth(0), atlasHeight(0), root(nullptr) { }
	virtual ~BinPacker()
	{
		// release the root
		if (root)
		{
			delete root;
			root = nullptr;
		}

		// release the rects
		for (TBinRect *rect : rects)
		{
			if(rect != nullptr)
				delete rect;
		}
		rects.clear();
	}
	
	void ForceSquare(bool allow) { forceSquare = allow; }
	void Add(TBinRect *input) { rects.push_back(input); }
	void Add(std::vector<TBinRect*> &input) { rects.insert(rects.begin(), input.begin(), input.end()); }
	
	void Remove(int index)
	{
		if(index < 0) index = 0;
		if(index > (int)rects.size() - 1) index = (int)rects.size() - 1;
		rects.erase(rects.begin() + index);
	}
	
	void Remove(int index, int count)
	{
		if(index < 0) index = 0;
		if(index > (int)rects.size() - 1) index = (int)rects.size() - 1;
		if(index + count > (int)rects.size() - 1) count = (int)rects.size() - index - 1;
		for(int i = index; i <index + count; ++i)
			delete rects[i];
		rects.erase(rects.begin() + index, rects.begin() + index + count);
	}
	
	void Clear()
	{
		for (TBinRect *binRect : rects)
			delete binRect;
		rects.clear();
	}
	
	void Commit()
	{
		int i = 0;
		bool solved = false;
		
		// real-world error-checking for when rects are supplied
		if(!rects.size())
		{
			std::cout << "ERROR: No BinRects present" << std::endl;
			return;
		}
		
		// make sure each rectangle has over zero units in both dimensions
		for(BinRect *rect : rects)
		{
			// make sure the rect's area is > 0.0f
			if(rect == nullptr || rect->GetArea() <= 0.0f)
			{
				std::cout << "ERROR: nullptr or invalid rect area" << std::endl;
				return;
			}
		}

		// run any pre-commit routines
		if (!PreCommit())
		{
			std::cout << "ERROR: PreCommit() failed" << std::endl;
			return;
		}
		
		// sort the rects, and insert them into the atlas using the algorithm --making the atlas large when needed
		sort(rects.rbegin(), rects.rend(), CompareBinRects<TBinRect>);
		while(!solved)
		{
			// determine the current altas size
			if(forceSquare)
			{
				atlasWidth = 1 << i;
				atlasHeight = 1 << i;
			} else {
				int shiftAmount = i / 2;
				atlasWidth = 1 << shiftAmount;
				atlasHeight = 1 << (i % 2 ? (shiftAmount + 1) : shiftAmount);
			}
			
			// allocate the root node, and insert each rect
			bool insertResult = false;
			root = new BinNode((float)atlasWidth, (float)atlasHeight, nullptr);
			for(int e=0;e<(int)rects.size();++e)
			{
				// only pack if allowed
				if(rects[e]->CanPack())
				{
					// attempt to insert the rect
					insertResult = root->InsertRect(rects[e], (float)padding);
					if(!insertResult)
						break;
				}
			}
			
			// release the current rects, and repeat
			if(!insertResult)
			{
				delete root;
				root = nullptr;
				++i;
				continue;
			}
			
			solved = true; // deem as solved if made to the end of the loop
		}
		PostCommit(); // run the post-commit routine
		
		// calculate unused space
		float spaceUsed = 0.0f;
		for(BinRect *rect : rects)
			spaceUsed += rect->GetPaddedArea((float)padding);
		spaceWasted = (atlasWidth * atlasHeight) - spaceUsed;
	}
	
	void SetPadding(int amount) { padding = amount < 0 ? 0 : amount; }
	
	bool IsForcingSquare() { return forceSquare; }
	int GetNumPackedRects() { int count = (int)rects.size(); for(int i=0;i<(int)rects.size();++i) --count; return count; }
	int GetNumRects() { return (int)rects.size(); }
	int GetAtlasWidth() { return atlasWidth; }
	int GetAtlasHeight() { return atlasHeight; }
	float GetSpaceWasted() { return spaceWasted; }
	BinNode *GetRoot() { return root; }
	TBinRect *GetRect(int index) { return rects[index]; }
};

#endif
