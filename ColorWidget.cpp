﻿#include "ColorWidget.h"

#include <QBrush>
#include <QPainter>
#include <QColorDialog>

#include <math.h>


ColorWidget::ColorWidget(QWidget *parent) : QWidget(parent)
{
	color.setRgb(255,255,255);
}

void ColorWidget::mouseDoubleClickEvent(QMouseEvent*)
{
	QColorDialog colorDialog;
	QColor temp = colorDialog.getColor(color, this, "Choose a Color", QColorDialog::ShowAlphaChannel);
	if(temp.isValid())
		color = temp;
	update();

	// convert QColor, and emit the event
	Color32f output(color.redF(), color.greenF(), color.blueF(), color.alphaF());
	emit ColorSelected(output);
}

void ColorWidget::paintEvent(QPaintEvent*)
{
	const QBrush blackBrush(Qt::black);
	const QBrush whiteBrush(Qt::white);

	QPainter painter(this);
	QBrush colorBrush(QColor(color.rgb()));
	QRect backgroundRect(0, 0, width(), height());
	QRect colorRect(0, 0, width() - 1, height() - 6);
	QRect alphaRect(0, height() - 5, (int)((float)width() * color.alphaF()), 5);

	// draw the color
	painter.fillRect(backgroundRect, blackBrush);
	painter.fillRect(colorRect, colorBrush);
	painter.drawRect(colorRect);
	painter.fillRect(alphaRect, whiteBrush);
}
