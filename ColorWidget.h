﻿#ifndef _COLOR_WIDGET_H_
#define _COLOR_WIDGET_H_

#include "Types.h"
#include <QColor>
#include <QWidget>


class ColorWidget : public QWidget
{
	Q_OBJECT

private:
	QColor color;

protected:
	void mouseDoubleClickEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);

signals:
	void ColorSelected(Color32f color);

public:
	explicit ColorWidget(QWidget *parent = 0);

	void SetColor(QColor color) { this->color = color; repaint(); }
	QColor GetColor() const { return color; }
};

#endif // _COLOR_WIDGET_H_
