#include "GLHelper.h"

#include <string>
#include <fstream>


GLuint helperSpriteShader = 0;


GLuint CompileShader(std::string source, ShaderType type)
{
	// convert source to a c string, and create the shader object
	const char *shaderSource = source.c_str();
	GLuint shader = glCreateShader((GLuint)type);
	GLint status = 0;

	// upload the shader source and compile
	glShaderSource(shader, 1, &shaderSource, nullptr);
	glCompileShader(shader);

	// check for compilation errors
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status)
	{
		// display the compiler log
		GLint logLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0)
		{
			// get the shader info log
			char *log = new char[logLength];
			glGetShaderInfoLog(shader, logLength, &logLength, log);

			// print error header based on shader type
			switch (type)
			{
				case ShaderType::Vertex:			printf(" - ERROR - Vertex Shader\n%s", log);			break;
				case ShaderType::TessControl:		printf(" - ERROR - TessControl Shader\n%s", log);		break;
				case ShaderType::TessEvaluation:	printf(" - ERROR - TessEvaluation Shader\n%s", log);	break;
				case ShaderType::Geometry:			printf(" - ERROR - Geometry Shader\n%s", log);			break;
				case ShaderType::Fragment:			printf(" - ERROR - Fragment\n%s", log);					break;
			}

			// print the contents of the log
			if (log)
			{
				delete[] log;
				log = nullptr;
			}

			printf("SHADER SOURCE:\n");
			printf("%s", source.c_str());
			printf("\n");
		}

		// delete the shader
		glDeleteShader(shader);
		shader = 0;
	}

	return shader;
}

GLuint CompileShader(std::string filename, std::string path, ShaderType type)
{
	// make sure there's a slash at the end of the path
	if(path.back() != '/')
		path += '/';

	std::ifstream inputFile((path + filename).c_str());
	std::string source((std::istreambuf_iterator<char>(inputFile)),
					   std::istreambuf_iterator<char>());

	GLuint result = CompileShader(source, type);
	inputFile.close();
	return result;
}

GLuint LinkShaders(GLuint *shaderHandles, int count)
{
	if(shaderHandles == nullptr)
	{
		printf("LinkShaders: shaderHandles is NULL\n");
		return 0;
	}

	// create program, and attach shaders
	GLuint program = glCreateProgram();
	for(int i=0;i<count;++i)
	{
		// make sure the handle is registered as a shader
		if (shaderHandles[i] > 0 || glIsShader(shaderHandles[i]))
			glAttachShader(program, shaderHandles[i]);
		else
			printf("LinkShaders: shader handle isn't valid (%i)\n", shaderHandles[i]);
	}

	// link the program, and detach all handles
	glLinkProgram(program);
	for(int i=0;i<count;++i)
	{
		// only detach if it's registered as a shader
		if(glIsShader(shaderHandles[i]))
			glDetachShader(program, shaderHandles[i]);
	}

	// check link status
	int status = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status)
	{
		char *log = nullptr;
		int logLength = 0;

		// get the program's info log
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0)
		{
			log = new char[logLength];
			glGetProgramInfoLog(program, logLength, &logLength, log);
			printf("ERROR - Link Log:\n%s", log);
			if (log)
			{
				delete[] log;
				log = nullptr;
			}
		}

		// delete the program
		glDeleteProgram(program);
		program = 0;
	}
	return program;
}

GLuint CreateBasicShader(std::string vertSource, std::string fragSource)
{
	// compile both shaders, and make sure they are both valid
	GLuint vertShader = CompileShader(vertSource, ShaderType::Vertex);
	GLuint fragShader = CompileShader(fragSource, ShaderType::Fragment);
	if(!vertShader || !fragShader)
	{
		// if not valid, make sure they're both deleted, and return zero
		ReleaseShader(&vertShader);
		ReleaseShader(&fragShader);
		return 0;
	}

	// create a vector of handles, link them, clear and return
	GLuint shaderHandles[2] { vertShader, fragShader };
	GLuint program = LinkShaders(shaderHandles, 2);
	ReleaseShader(&vertShader);
	ReleaseShader(&fragShader);
	return program;
}

GLuint CreateBasicShader(std::string vertFilename, std::string fragFilename, std::string path)
{
	// make sure there's a slash at the end of the path
	if(path.back() != '/')
		path += '/';

	std::ifstream inputVertFile((path + vertFilename).c_str());
	std::string vertSource((std::istreambuf_iterator<char>(inputVertFile)),
						   std::istreambuf_iterator<char>());
	std::ifstream inputFragFile((path + fragFilename).c_str());
	std::string fragSource((std::istreambuf_iterator<char>(inputFragFile)),
						   std::istreambuf_iterator<char>());

	return CreateBasicShader(vertSource, fragSource);
}

void ReleaseShader(GLuint *shader)
{
	// make sure the handle is pointing to a shader before deletion
	if (shader != nullptr && *shader != 0 && glIsShader(*shader))
	{
		glDeleteProgram(*shader);
		*shader = 0;
	}
}

void ReleaseProgram(GLuint *program)
{
	// make sure the handle is pointing to a program before deletion
	if (program != nullptr && *program != 0 && glIsProgram(*program))
	{
		glDeleteProgram(*program);
		*program = 0;
	}
}
