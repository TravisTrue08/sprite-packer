#ifndef _GL_HELPER_H_
#define _GL_HELPER_H_

#include <GL/glew.h>
#include <string>

#include "Types.h"


enum class ShaderType
{
	Vertex = GL_VERTEX_SHADER,
	TessControl = GL_TESS_CONTROL_SHADER,
	TessEvaluation = GL_TESS_EVALUATION_SHADER,
	Geometry = GL_GEOMETRY_SHADER,
	Fragment = GL_FRAGMENT_SHADER
};


struct SpriteVertex
{
	Vector2 position;
	Vector2 coord;

	SpriteVertex() { }
	SpriteVertex(float x, float y, float u, float v)
	{
		position.Set(x, y);
		coord.Set(u, v);
	}

	void Set(float x, float y, float u, float v)
	{
		position.Set(x, y);
		coord.Set(u, v);
	}
};


GLuint CompileShader(std::string source, ShaderType type);
GLuint CompileShader(std::string filename, std::string path, ShaderType type);
GLuint LinkShaders(GLuint *shaderHandles, int count);
GLuint CreateBasicShader(std::string vertSource, std::string fragSource);
GLuint CreateBasicShader(std::string vertFilename, std::string fragFilename, std::string path);
GLuint CompileShader(std::string vertFilename, std::string fragFilename, std::string path);
void ReleaseShader(GLuint *shader);
void ReleaseProgram(GLuint *program);

#endif // _GL_HELPER_H_
