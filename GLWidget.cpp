#include "GLWidget.h"
#include "SpritePacker.h"

#include <algorithm>
#include <QItemSelection>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QDebug>


GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent),
	vao(0), vbo(0), atlasVBO(0), selectionVBO(0),
	backgroundProgram(0), selectionProgram(0),
	backgroundZoomLoc(-1), backgroundHalfViewportLoc(-1),
	selectionZoomLoc(-1), selectionOrthoLoc(-1),
	zoomFactor(1.0f),
	spritePacker(nullptr)
{
}

GLWidget::~GLWidget()
{
	selectedIndexes.clear();
	SpritePacker::Shutdown();
	ReleaseProgram(&backgroundProgram);
	ReleaseProgram(&selectionVBO);
	if(atlasVBO) glDeleteBuffers(1, &atlasVBO);
	if(selectionVBO) glDeleteBuffers(1, &selectionVBO);
	if(vbo) glDeleteBuffers(1, &vbo);
	if(vao) glDeleteVertexArrays(1, &vao);
}

void GLWidget::GenerateSelection()
{
	if(!spritePacker)
		return;

	// update the VBO if there's a selection
	int numRects = (int)selectedIndexes.size();
	if(numRects > 0)
	{
		int vertexSize = sizeof(float) * 8;
		int indexSize = sizeof(unsigned short) * 6;
		int indexBufferSize = indexSize * numRects;
		int vertexBufferSize = vertexSize * numRects;
		int size = indexBufferSize + vertexBufferSize;
        unsigned short indices[6] = { 0, 1, 2, 0, 2, 3 };

		// regenerate the selection VBO
		glBindBuffer(GL_ARRAY_BUFFER, selectionVBO);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);

		// generate the rects
		for(int i=0;i<numRects;++i)
		{
			int indexOffset = i * 4;
			SpriteBinRect rect = *spritePacker->GetRect(selectedIndexes[i]);
			rect.position -= halfAtlasSize;

            // set the indices
            indices[0] = 0 + indexOffset;
            indices[1] = 1 + indexOffset;
            indices[2] = 2 + indexOffset;
            indices[3] = 0 + indexOffset;
            indices[4] = 2 + indexOffset;
            indices[5] = 3 + indexOffset;

			float vertices[8] = {
				rect.size.x + rect.position.x + 1.0f,	rect.position.y,
				rect.position.x,						rect.position.y,
				rect.position.x,						rect.size.y + rect.position.y + 1.0f,
				rect.size.x + rect.position.x + 1.0f,	rect.size.y + rect.position.y + 1.0f
			};

			// add the index and vertex data
			glBufferSubData(GL_ARRAY_BUFFER, vertexSize * i, vertexSize, vertices);
			glBufferSubData(GL_ARRAY_BUFFER, (vertexSize * numRects) + (indexSize * i), indexSize, indices);
		}
	}
	update();
}

void GLWidget::ProcessDrag(Vector2 position)
{
	// make sure the sprite packer is valid
	if(spritePacker)
	{
		Vector2 halfZoomAtlasSize;
		Vector2 halfViewportSize = viewportSize / 2.0f;
		halfZoomAtlasSize.x = spritePacker->GetAtlasWidth() * zoomFactor * 0.5f;
		halfZoomAtlasSize.y = spritePacker->GetAtlasHeight() * zoomFactor * 0.5f;

		// check if the image is draggable along the x-axis
		if(halfZoomAtlasSize.x > halfViewportSize.x)
		{
			float maxOffset = (halfZoomAtlasSize.x - halfViewportSize.x) / halfViewportSize.x;
			viewOffset.x += (position.x - lastMousePos.x) / halfViewportSize.x;
			if(viewOffset.x >  maxOffset) viewOffset.x =  maxOffset;
			if(viewOffset.x < -maxOffset) viewOffset.x = -maxOffset;
		} else viewOffset.x = 0.0f;

		// check if the image is draggable along the y-axis
		if(halfZoomAtlasSize.y > halfViewportSize.y)
		{
			float maxOffset = (halfZoomAtlasSize.y - halfViewportSize.y) / halfViewportSize.y;
			viewOffset.y += (position.y - lastMousePos.y) / halfViewportSize.y;
			if(viewOffset.y >  maxOffset) viewOffset.y =  maxOffset;
			if(viewOffset.y < -maxOffset) viewOffset.y = -maxOffset;
		} else viewOffset.y = 0.0f;

		// update the ortho matrices, and the mouse's last position
		orthoMat.m[12] =  viewOffset.x;
		orthoMat.m[13] = -viewOffset.y;
		spriteOrthoMat.m[12] =  viewOffset.x;
		spriteOrthoMat.m[13] = -viewOffset.y;
		lastMousePos = position;
	}
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
	if(event->buttons() == Qt::LeftButton)
		lastMousePos.Set(event->x(), event->y());
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
	ProcessDrag(Vector2(event->x(), event->y()));
	update();
}

void GLWidget::wheelEvent(QWheelEvent *event)
{
	emit MouseScrolled((float)event->delta() / 100.0f);
}

void GLWidget::initializeGL()
{
	float backgroundVertices[8] = {
		 1.0f,  1.0f,
		-1.0f,  1.0f,
		-1.0f, -1.0f,
		 1.0f, -1.0f,
	};

	unsigned char indices[6] = { 0, 1, 2, 0, 2, 3 };

	std::string backgroundVertSource =
			"#version 330\n"
			"layout(location = 0) in vec4 in_position;"
			"void main()"
			"{"
			"	gl_Position = in_position;"
			"}";

	std::string backgroundFragSource =
			"#version 330\n"
			"out vec4 out_color;"
			"uniform float u_zoom;"
			"uniform vec2 u_viewportHalfSize;"
			"const float tile_size = 25.0;"
			"const vec4 dark_gray = vec4(0.5, 0.5, 0.5, 1.0);"
			"const vec4 light_gray = vec4(0.75, 0.75, 0.75, 1.0);"
			"void main()"
			"{"
			"	float x = (gl_FragCoord.x - u_viewportHalfSize.x);"
			"	float y = (gl_FragCoord.y - u_viewportHalfSize.y);"
			"	int x_tile = int(x / (tile_size * u_zoom));"
			"	int y_tile = int(y / (tile_size * u_zoom));"
			"	if(x < 0) --x_tile;"
			"	if(y < 0) --y_tile;"
			"	if(bool(y_tile % 2))"
			"	{"
			"		if(bool(x_tile % 2))"
			"			out_color = dark_gray;"
			"		else"
			"			out_color = light_gray;"
			"	} else {"
			"		if(bool(x_tile % 2))"
			"			out_color = light_gray;"
			"		else"
			"			out_color = dark_gray;"
			"	}"
			"}";

	std::string selectionVertSource =
			"#version 330\n"
			"layout(location = 0) in vec4 in_position;"
			"uniform float u_zoom;"
			"uniform mat4 u_orthoMat;"
			"void main()"
			"{"
			"	gl_Position = u_orthoMat * vec4(in_position.xy * u_zoom, 0.0, 1.0f);"
			"}";

	std::string selectionFragSource =
			"#version 330\n"
			"out vec4 out_color;"
			"void main()"
			"{"
			"	out_color = vec4(0.3, 0.3, 0.6, 0.5);"
			"}";

	// initialize GLEW
	glewExperimental = GL_TRUE;
	int result = glewInit();
	if(result != GLEW_OK)
	{
		fprintf(stderr, "unable to initialize GLEW (%i)\n", result);
		exit(2);
	}

	// initialize OpenGL
	qDebug() << "OpenGL Version: " << (const char*)glGetString(GL_VERSION);
	qDebug() << "OpenGL Renderer: " << (const char*)glGetString(GL_RENDERER);
	glGetError(); // flush out potential 1280 error that happens with GLEW sometimes
	SpritePacker::Initialize();

	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	// generate and bind the VAO
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// setup background's VBO
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, (sizeof(float)) * 8 + (sizeof(unsigned char) * 6), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 8, backgroundVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 8, sizeof(unsigned char) * 6, indices);

	// setup the atlas' VBO
	glGenBuffers(1, &atlasVBO);
	glBindBuffer(GL_ARRAY_BUFFER, atlasVBO);
	glBufferData(GL_ARRAY_BUFFER, (sizeof(SpriteVertex) * 4) + (sizeof(unsigned char) * 6), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(SpriteVertex) * 4, sizeof(unsigned char) * 6, indices);

	// setup the selection's VBO
	glGenBuffers(1, &selectionVBO);
	glBindBuffer(GL_ARRAY_BUFFER, selectionVBO);

	// setup the background shader, and set the uniforms
	backgroundProgram = CreateBasicShader(backgroundVertSource, backgroundFragSource);
	backgroundZoomLoc = glGetUniformLocation(backgroundProgram, "u_zoom");
	backgroundHalfViewportLoc = glGetUniformLocation(backgroundProgram, "u_viewportHalfSize");

	// setup the background shader, and set the uniforms
	selectionProgram = CreateBasicShader(selectionVertSource, selectionFragSource);
	selectionZoomLoc = glGetUniformLocation(selectionProgram, "u_zoom");
	selectionOrthoLoc = glGetUniformLocation(selectionProgram, "u_orthoMat");
}

void GLWidget::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);
	viewportSize.Set(width, height);
	float halfWidth = (float)width / 2.0f;
	float halfHeight = (float)height / 2.0f;
	orthoMat = Matrix4::Ortho(-halfWidth, halfWidth, halfHeight, -halfHeight, -10.0f, 10.0f);
	spriteOrthoMat = Matrix4::Ortho(-halfWidth, halfWidth, -halfHeight, halfHeight, -10.0f, 10.0f);
}

void GLWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw the background
	glUseProgram(backgroundProgram);
	glUniform1f(backgroundZoomLoc, zoomFactor);
	glUniform2f(backgroundHalfViewportLoc, (float)(width() / 2), (float)(height() / 2));
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, (void*)(sizeof(float) * 8));

	// draw the atlas
	if(spritePacker && spritePacker->GetAtlasTexture() && spritePacker->GetNumRects())
	{
		// bind the texture, the program and upload uniforms
		glBindTexture(GL_TEXTURE_2D, spritePacker->GetAtlasTexture());
		glUseProgram(spritePacker->GetSpriteProgram());
		glUniform1i(spritePacker->GetTextureLoc(), 0);
		glUniform1f(spritePacker->GetZoomLoc(), zoomFactor);
		glUniformMatrix4fv(spritePacker->GetOrthoMatLoc(), 1, GL_FALSE, orthoMat.m);

		glBindBuffer(GL_ARRAY_BUFFER, atlasVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, atlasVBO);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), 0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), (void*)sizeof(Vector2));
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, (void*)(sizeof(SpriteVertex) * 4));
	}

	// draw the selection
	if(selectedIndexes.size() > 0)
	{
		glUseProgram(selectionProgram);
		glUniform1f(selectionZoomLoc, zoomFactor);
		glUniformMatrix4fv(selectionOrthoLoc, 1, GL_FALSE, spriteOrthoMat.m);

		glBindBuffer(GL_ARRAY_BUFFER, selectionVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, selectionVBO);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDrawElements(GL_TRIANGLES, (int)selectedIndexes.size() * 6, GL_UNSIGNED_SHORT, (void*)(sizeof(float) * (int)selectedIndexes.size() * 8));
	}
}

void GLWidget::SelectionOccurred(const QItemSelection &selection, const QItemSelection &deselected)
{
	// add all newly-selected indexes to the list
	for(QModelIndex index : selection.indexes())
		selectedIndexes.push_back(index.row());

	// remove any unselected indexes from the list
	for(QModelIndex index : deselected.indexes())
		selectedIndexes.erase(std::remove(selectedIndexes.begin(), selectedIndexes.end(), index.row()), selectedIndexes.end());
	GenerateSelection();
}

void GLWidget::UpdateAtlasVBO()
{
	// make sure the sprite packer is valid
	if(spritePacker)
	{
		// get the half-dimensions
		halfAtlasSize.x = (float)spritePacker->GetAtlasWidth() / 2.0f;
		halfAtlasSize.y = (float)spritePacker->GetAtlasHeight() / 2.0f;
		SpriteVertex vertices[4];

		// set the vertex data
		vertices[0].Set( halfAtlasSize.x,  halfAtlasSize.y, 1.0f, 1.0f);
		vertices[1].Set(-halfAtlasSize.x,  halfAtlasSize.y, 0.0f, 1.0f);
		vertices[2].Set(-halfAtlasSize.x, -halfAtlasSize.y, 0.0f, 0.0f);
		vertices[3].Set( halfAtlasSize.x, -halfAtlasSize.y, 1.0f, 0.0f);

		// set the data to the buffer
		glBindBuffer(GL_ARRAY_BUFFER, atlasVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(SpriteVertex) * 4, vertices);
		update();
	}
}

void GLWidget::ZoomUpdated(float zoom)
{
	zoomFactor = zoom;
	lastMousePos.Set(0.0f, 0.0f);
	ProcessDrag(Vector2());
	update();
}
