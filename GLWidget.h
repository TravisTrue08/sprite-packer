#ifndef _GL_WIDGET_H_
#define _GL_WIDGET_H_

#include "Matrix.h"
#include "GLHelper.h"
#include "ZoomWidget.h"

#include <map>
#include <QOpenGLWidget>


class SpritePacker;
class SpriteBinRect;
class QItemSelection;


class GLWidget : public QOpenGLWidget
{
	Q_OBJECT

private:
	GLuint vao;
	GLuint vbo;
	GLuint atlasVBO;
	GLuint selectionVBO;
	GLuint backgroundProgram;
	GLuint selectionProgram;
	GLint backgroundZoomLoc;
	GLint backgroundHalfViewportLoc;
	GLint selectionZoomLoc;
	GLint selectionOrthoLoc;
	Matrix4 orthoMat;
	Matrix4 spriteOrthoMat;
	Vector2 halfAtlasSize;
	Vector2 lastMousePos;
	Vector2 viewportSize;
	Vector2 viewOffset;
	float zoomFactor;

	std::vector<int> selectedIndexes;

	SpritePacker *spritePacker;

	void GenerateSelection();
	void ProcessDrag(Vector2 position);

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);

	void initializeGL();
	void resizeGL(int width, int height);
	void paintGL();

public slots:
	void SelectionOccurred(const QItemSelection &selection, const QItemSelection &deselected);
	void UpdateAtlasVBO();
	void ZoomUpdated(float zoom);

public:
	explicit GLWidget(QWidget *parent = 0);
	~GLWidget();

	void SetSpritePacker(SpritePacker *spritePacker) { this->spritePacker = spritePacker; }

signals:
	void MouseScrolled(float increment);
};

#endif // GL_WIDGET_H_
