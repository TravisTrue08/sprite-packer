#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "SpriteItemModel.h"
#include "SpritePacker.h"

#include <FreeImage/FreeImage.h>

#include <QMessageBox>
#include <QFileDialog>

#include <assert.h>


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	spritePacker(nullptr),
	model(nullptr),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	spritePacker = new SpritePacker();
	model = new SpriteItemModel(this, spritePacker);
	ui->spriteListView->setModel(model);
	ui->backgroundColor->SetColor(QColor(0, 0, 0, 0));
	ui->glWidget->SetSpritePacker(spritePacker);

	connect(model, SIGNAL(Committed()), ui->glWidget, SLOT(UpdateAtlasVBO()));
	connect(model, SIGNAL(Committed()), this, SLOT(UpdateAtlasSizeDisplay()));
	connect(ui->backgroundColor, SIGNAL(ColorSelected(Color32f)), this, SLOT(UpdateBackgroundColor(Color32f)));
	connect(ui->spriteListView, SIGNAL(SelectionOccurred(const QItemSelection&, const QItemSelection&)),
			ui->glWidget, SLOT(SelectionOccurred(const QItemSelection&, const QItemSelection&)));
}

MainWindow::~MainWindow()
{
	if(model) delete model;
	delete ui;
}

void MainWindow::UpdateBackgroundColor(Color32f color)
{
	// update the sprite packer's render target clear color, then re-commit
	spritePacker->SetBackgroundColor(color);
	spritePacker->Commit();
	ui->glWidget->update();
}

void MainWindow::UpdateAtlasSizeDisplay()
{
	std::string message = std::string("Atlas Size: ") + std::to_string(spritePacker->GetAtlasWidth()) +
													"x" + std::to_string(spritePacker->GetAtlasHeight());
	ui->statusBar->showMessage(message.c_str());
}

void MainWindow::on_action_New_triggered()
{
	// bring up a message box, and clear if the user confirms
	QMessageBox::StandardButton result = QMessageBox::question(this,
															   "New Atlas",
															   "Are you sure you want to reset the scene?",
															   QMessageBox::Yes | QMessageBox::No);
	if(model && result == QMessageBox::Yes)
	{
		model->removeRows(0, spritePacker->GetNumRects());
		ui->glWidget->update();
	}
}

void MainWindow::on_action_Save_triggered()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Save Atlas", "", "Portable Network Graphic (*.png)");
	spritePacker->Export(std::string(filePath.toUtf8()));
}

void MainWindow::on_actionE_xit_triggered()
{
	close();
}

void MainWindow::on_forceSquareCheckbox_toggled(bool checked)
{
	// make sure the sprite packer is valid
	if(spritePacker)
	{
		// force the sprite packer to make a square atlas, commit and update the renderer widget's display
		spritePacker->ForceSquare(checked);
		spritePacker->Commit();
		ui->glWidget->UpdateAtlasVBO();
		UpdateAtlasSizeDisplay();
	}
}

void MainWindow::on_paddingSpinner_valueChanged(int value)
{
	// make sure the sprite packer is valid
	if(spritePacker)
	{
		// set the packer's padding, commit and update the atlas in the renderer widget's display
		spritePacker->SetPadding(value);
		spritePacker->Commit();
		ui->glWidget->UpdateAtlasVBO();
		UpdateAtlasSizeDisplay();
	}
}
