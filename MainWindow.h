#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Types.h"
#include <QMainWindow>


class SpritePacker;
class SpriteItemModel;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	SpritePacker *spritePacker;
	SpriteItemModel *model;
	Ui::MainWindow *ui;

private slots:
	void UpdateBackgroundColor(Color32f color);
	void UpdateAtlasSizeDisplay();
	void on_action_New_triggered();
	void on_action_Save_triggered();
	void on_actionE_xit_triggered();
	void on_forceSquareCheckbox_toggled(bool checked);
	void on_paddingSpinner_valueChanged(int value);

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
};

#endif // MAINWINDOW_H
