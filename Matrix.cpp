#include "Matrix.h"

#include <stdio.h>


Matrix4::Matrix4()
{
	m[ 0] = 1.0f;	m[ 4] = 0.0f;	m[ 8] = 0.0f;	m[12] = 0.0f;
	m[ 1] = 0.0f;	m[ 5] = 1.0f;	m[ 9] = 0.0f;	m[13] = 0.0f;
	m[ 2] = 0.0f;	m[ 6] = 0.0f;	m[10] = 1.0f;	m[14] = 0.0f;
	m[ 3] = 0.0f;	m[ 7] = 0.0f;	m[11] = 0.0f;	m[15] = 1.0f;
}

Matrix4 Matrix4::Ortho(float left, float right, float top, float bottom, float zNear, float zFar)
{
	Matrix4 resultMat;
	const float tx = -(right + left) / (right - left);
	const float ty = -(top + bottom) / (top - bottom);
	const float tz = -(zFar + zNear) / (zFar - zNear);
	
	resultMat.m[ 0] = 2.0f / (right - left);
	resultMat.m[ 5] = 2.0f / (top - bottom);
	resultMat.m[10] =-2.0f / (zFar - zNear);
	resultMat.m[12] = tx;
	resultMat.m[13] = ty;
	resultMat.m[14] = tz;
	return resultMat;
}
