#ifndef _MATRIX_H_
#define _MATRIX_H_


class Matrix4
{
public:
	float m[16];
	
	Matrix4();

	static Matrix4 Ortho(float width, float height, float zNear, float zFar) { return Ortho(0.0f, width, height, 0.0f, zNear, zFar); }
	static Matrix4 Ortho(float left, float right, float top, float bottom, float zNear, float zFar);
};

#endif
