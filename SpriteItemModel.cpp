#include "SpriteItemModel.h"
#include "SpritePacker.h"

#include <FreeImage/FreeImage.h>

#include <QMimeData>
#include <QUrl>


SpriteItemModel::SpriteItemModel(QObject *parent, SpritePacker *spritePacker) : QAbstractListModel(parent), spritePacker(spritePacker)
{
}

Qt::ItemFlags SpriteItemModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index) | Qt::ItemIsDropEnabled;
}

QVariant SpriteItemModel::data(const QModelIndex &index, int role) const
{
	// make sure the sprite packer is valid
	if(spritePacker)
	{
		// make sure the index is valid, the role is displaying text and the row index within numRects range
		if(index.isValid() && role == Qt::DisplayRole && index.row() < spritePacker->GetNumRects())
		{
			SpriteBinRect *rect = spritePacker->GetRect(index.row());
			return QString(rect->GetFilename().c_str());
		}
	}
	return QVariant();
}

bool SpriteItemModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int, const QModelIndex&)
{
	// make sure relevant data is valid
	if(!spritePacker || !data || action == Qt::IgnoreAction)
		return false;

	// get rowCount if row is invalid (this might be an old Qt bug)
	if(row == -1)
		row = rowCount();

	QString filePath;
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;

	// populate the fileList with the current rects' filePaths
	for(int i=0;i<spritePacker->GetNumRects();++i)
		tempFilePaths << spritePacker->GetRect(i)->GetFilePath().c_str();

	// make sure the MIME data has URLs
	if(data->hasUrls())
	{
		// loop through all URLs, and make sure each of them are valid image files supported by FreeImage
		foreach(QUrl url, data->urls())
		{
            // set the file path to the trimmed path (without preceeding slash) for Windows
			#ifdef PLATFORM_WINDOWS
			filePath = url.path().mid(1);
            #else
			filePath = url.path();
            #endif

            tempFilePaths << filePath; // add it to the tempFilePaths

			// attempt to get the file's type
			fif = FreeImage_GetFileType(filePath.toUtf8(), 0);
			if(fif == FIF_UNKNOWN) fif = FreeImage_GetFIFFromFilename(filePath.toUtf8());

			// if unknown, ignore the event and break
			if(fif == FIF_UNKNOWN)
				return false;

			// check that the plugin has reading capabilities and load the file
			if(!FreeImage_FIFSupportsReading(fif))
				return false;
		}
	} else return false;

	// remove duplicates, and all filenames that current exist
	for(int i=0;i<spritePacker->GetNumRects();++i)
		tempFilePaths.removeAll(spritePacker->GetRect(i)->GetFilePath().c_str());

	// insert the rows
	int count = tempFilePaths.count();
	if(count > 0)
		insertRows(row, tempFilePaths.count(), QModelIndex());
	tempFilePaths.clear();
	return true;
}

bool SpriteItemModel::canDropMimeData(const QMimeData*, Qt::DropAction, int, int, const QModelIndex&) const
{
	return true;
}

bool SpriteItemModel::insertRows(int row, int count, const QModelIndex&)
{
	if(!spritePacker)
		return false;

	// insert the new rows
	beginInsertRows(QModelIndex(), row, row+count-1);
	for(QString filePath : tempFilePaths)
	{
		SpriteBinRect *rect = SpriteBinRect::Create(std::string(filePath.toUtf8()));
		if(rect) spritePacker->Add(rect);
	}
	endInsertRows();

	// update changes
	spritePacker->Commit();
	emit Committed();
	return true;
}

bool SpriteItemModel::removeRows(int row, int count, const QModelIndex&)
{
	if(!spritePacker)
		return false;

	if(row < 0) return false;
	int endIndex = row + count - 1;

	// insert the new rows
	beginRemoveRows(QModelIndex(), row, endIndex);
	for(int i=endIndex;i>=row;--i)
		spritePacker->Remove(i);
	endRemoveRows();

	// update changes
	spritePacker->Commit();
	emit Committed();
	return true;
}

int SpriteItemModel::rowCount(const QModelIndex&) const
{
	return spritePacker->GetNumRects();
}
