#ifndef _SPRITE_ITEM_MODEL_H_
#define _SPRITE_ITEM_MODEL_H_

#include <QAbstractListModel>


class SpritePacker;


class SpriteItemModel : public QAbstractListModel
{
	Q_OBJECT

private:
	SpritePacker *spritePacker;
	QStringList tempFilePaths;

protected:
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

	bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;

public:
	SpriteItemModel(QObject *parent, SpritePacker *spritePacker);

	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

signals:
	void Committed();
};

#endif // SPRITEITEMMODEL_H
