#include "SpriteListView.h"

#include <QListIterator>
#include <QKeyEvent>


SpriteListView::SpriteListView(QWidget *parent) : QListView(parent)
{
}

void SpriteListView::keyPressEvent(QKeyEvent *event)
{
	// delete any selected items
	QModelIndexList indexList = selectionModel()->selectedIndexes();
	if(event->key() == Qt::Key_Delete && indexList.count() > 0)
	{
		// get the setup a reverse iterator for the selected indexes
		qSort(indexList.begin(), indexList.end());
		QListIterator<QModelIndex> iter(indexList);
		iter.toBack();

		// iterate backwards
		setUpdatesEnabled(false);
		while(iter.hasPrevious())
		{
			// get the indes, and delete it
			QModelIndex index = iter.previous();
			model()->removeRow(index.row(), index);
		}
		setUpdatesEnabled(true);
	}
}

void SpriteListView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	emit SelectionOccurred(selected, deselected);
    QListView::selectionChanged(selected, deselected);
}
