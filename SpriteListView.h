#ifndef _SPRITE_LIST_VIEW_H_
#define _SPRITE_LIST_VIEW_H_

#include <QListView>


class SpriteListView : public QListView
{
	Q_OBJECT

protected:
	void keyPressEvent(QKeyEvent *event);
	void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

signals:
	void SelectionOccurred(const QItemSelection &selection, const QItemSelection &deselected);

public:
	SpriteListView(QWidget *parent = 0);
};

#endif // _SPRITE_LIST_VIEW_H_
