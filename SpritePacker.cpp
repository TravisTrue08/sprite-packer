#include "SpritePacker.h"
#include "Matrix.h"

#include <FreeImage/FreeImage.h>


bool SpritePacker::initialized = false;
GLint SpritePacker::orthoMatLoc = -1;
GLint SpritePacker::zoomLoc = -1;
GLint SpritePacker::textureLoc = -1;
GLuint SpritePacker::spriteProgram = 0;


template <class T> void INPLACESWAP(T& a, T& b)
{
	a ^= b; b ^= a; a ^= b;
}


bool SwapRedBlue32(FIBITMAP* dib)
{
	if (FreeImage_GetImageType(dib) != FIT_BITMAP)
		return false;

	const unsigned bytesperpixel = FreeImage_GetBPP(dib) / 8;
	if (bytesperpixel > 4 || bytesperpixel < 3)
		return false;

	const unsigned height = FreeImage_GetHeight(dib);
	const unsigned pitch = FreeImage_GetPitch(dib);
	const unsigned lineSize = FreeImage_GetLine(dib);

	BYTE* line = FreeImage_GetBits(dib);
	for (unsigned y = 0; y < height; ++y, line += pitch)
	{
		for (BYTE* pixel = line; pixel < line + lineSize; pixel += bytesperpixel)
			INPLACESWAP(pixel[0], pixel[2]);
	}

	return true;
}

void SpritePacker::Initialize()
{
	std::string spriteVertSource =
			"#version 330\n"
			"layout(location = 0) in vec2 in_position;"
			"layout(location = 1) in vec2 in_coord;"
			"out vec2 frag_coord;"
			"uniform float u_zoom;"
			"uniform mat4 u_orthoMat;"
			"void main()"
			"{"
			"	frag_coord = in_coord;"
			"	gl_Position = u_orthoMat * vec4(in_position.xy * u_zoom, 0.0, 1.0f);"
			"}";

	std::string spriteFragSource =
			"#version 330\n"
			"in vec2 frag_coord;"
			"out vec4 out_color;"
			"uniform sampler2D u_texture;"
			"void main()"
			"{"
			"	out_color = texture(u_texture, frag_coord);"
			"}";

	if(initialized) return;
	spriteProgram = CreateBasicShader(spriteVertSource, spriteFragSource);
	orthoMatLoc = glGetUniformLocation(spriteProgram, "u_orthoMat");
	zoomLoc = glGetUniformLocation(spriteProgram, "u_zoom");
	textureLoc = glGetUniformLocation(spriteProgram, "u_texture");
	initialized = true;
}

void SpritePacker::Shutdown()
{
	if(!initialized) return;
	ReleaseProgram(&spriteProgram);
	initialized = false;
}


SpriteBinRect *SpriteBinRect::Create(std::string filePath)
{
	SpriteBinRect *spriteBinRect = nullptr;
    void *data = nullptr;
    FIBITMAP *dib = nullptr;
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	int width, height, bitsPerPixel, size;

	// attempt to open the file
	fif = FreeImage_GetFileType(filePath.c_str(), 0);
	if(fif == FIF_UNKNOWN) fif = FreeImage_GetFIFFromFilename(filePath.c_str());
	if(fif == FIF_UNKNOWN)
	{
		fprintf(stderr, "couldn't resolve file type for %s\n", filePath.c_str());
		return nullptr;
	}

	// check that the plugin has reading capabilities and load the file
	if(FreeImage_FIFSupportsReading(fif))
        dib = FreeImage_Load(fif, filePath.c_str());
    if(!dib)
	{
		fprintf(stderr, "invalid type for %s\n", filePath.c_str());
		return nullptr;
	}

	// retrieve the image data
    FreeImage_FlipVertical(dib);
    data = FreeImage_GetBits(dib);
    width = FreeImage_GetWidth(dib);
    height = FreeImage_GetHeight(dib);
    bitsPerPixel = FreeImage_GetBPP(dib);
    FreeImage_ConvertToType(dib, FIT_UINT32);
	size = width * height * (bitsPerPixel / 8);
    SwapRedBlue32(dib);

	// if this somehow one of these failed (they shouldn't), return failure
	if(!data || width < 2 || height < 2)
	{
		fprintf(stderr, "invalid data for %s\n", filePath.c_str());
		return nullptr;
	}

	// generate the bin rect based on bpp
	switch(bitsPerPixel)
	{
	case 32:	spriteBinRect = new SpriteBinRect(filePath, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);	break;
	case 24:	spriteBinRect = new SpriteBinRect(filePath, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);		break;
	case 16:	spriteBinRect = new SpriteBinRect(filePath, width, height, GL_RGBA, GL_UNSIGNED_SHORT, data);	break;
	case 8:		spriteBinRect = new SpriteBinRect(filePath, width, height, GL_R, GL_UNSIGNED_BYTE, data);		break;
	}

	// release the FreeImage data
    FreeImage_Unload(dib);
	return spriteBinRect;
}

SpriteBinRect::SpriteBinRect(std::string filePath, GLint width, GLint height, GLenum imageFormat, GLenum type, void *data) : BinRect((float)width, (float)height)
{
	this->filePath = filePath;

	// allocate and setup the texture
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, imageFormat, type, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// generate the VBO
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

SpriteBinRect::~SpriteBinRect()
{
	if(vbo) glDeleteBuffers(1, &vbo);
	if(texture) glDeleteTextures(1, &texture);
}

void SpriteBinRect::GenerateVertices()
{
	// don't generate vertices for sprites that can't be packed
	if(!CanPack())
		return;

	SpriteVertex vertices[4];

	// generate vertices based on rotation
	if (IsRotated())
	{
		vertices[0].Set(size.x + position.x,	position.y,				1.0f, 1.0f);
		vertices[1].Set(position.x,				position.y,				1.0f, 0.0f);
		vertices[2].Set(position.x,				size.y + position.y,	0.0f, 0.0f);
		vertices[3].Set(size.x + position.x,	size.y + position.y,	0.0f, 1.0f);
	} else {
		vertices[0].Set(size.x + position.x,	position.y,				1.0f, 0.0f);
		vertices[1].Set(position.x,				position.y,				0.0f, 0.0f);
		vertices[2].Set(position.x,				size.y + position.y,	0.0f, 1.0f);
		vertices[3].Set(size.x + position.x,	size.y + position.y,	1.0f, 1.0f);
	}

	// bind, and generate VBO data
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(SpriteVertex) * 4, vertices, GL_STATIC_DRAW);
}


SpritePacker::SpritePacker() : atlasTexture(0)
{
	backgroundColor.Set(0.0f, 0.0f, 0.0f, 0.0f);
}

SpritePacker::~SpritePacker()
{
	if(atlasTexture) glDeleteTextures(1, &atlasTexture);
}

void SpritePacker::PostCommit()
{
	GLuint vao = 0;
	GLuint fbo = 0;
	Matrix4 orthoMat;
	int viewport[4] = { 0, 0, 0, 0 };

	// bind the VAO to generate each rect's VBO
	for (SpriteBinRect *rect : rects)
		rect->GenerateVertices();

	// release the currently-bound atlas textures
	if(atlasTexture)
	{
		glDeleteTextures(1, &atlasTexture);
		atlasTexture = 0;
	}

	// generate the test texture
	glGenTextures(1, &atlasTexture);
	glBindTexture(GL_TEXTURE_2D, atlasTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, atlasWidth, atlasHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// configure the framebuffer
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, atlasTexture, 0);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	// check if the framebuffer is complete
	GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (result != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(stderr, "ERROR: framebuffer not complete(%i)\n", result);
		return;
	}

	// set the viewport and clear the framebuffer
	glGetIntegerv(GL_VIEWPORT, viewport);
	glViewport(0, 0, atlasWidth, atlasHeight);
	glClearColor(backgroundColor.red, backgroundColor.green, backgroundColor.blue, backgroundColor.alpha);
	glClear(GL_COLOR_BUFFER_BIT);

	// generate, and bind the VAO
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// setup the shader
	orthoMat = Matrix4::Ortho(0.0f, (float)GetAtlasWidth(), 0.0f, (float)GetAtlasHeight(),-10.0f, 10.0f);
	glUseProgram(spriteProgram);
	glUniformMatrix4fv(orthoMatLoc, 1, GL_FALSE, orthoMat.m);
	glUniform1f(zoomLoc, 1.0f);
	glUniform1i(textureLoc, 0);

	// draw all sprites in the array
	for (SpriteBinRect *rect : rects)
	{
		// make sure the rect is valid, and can be packed
		if (rect == nullptr || !rect->CanPack())
			continue;

		// bind the rect's texture, its VBO, reset pointers and draw
		glBindTexture(GL_TEXTURE_2D, rect->GetTexture());
		glBindBuffer(GL_ARRAY_BUFFER, rect->GetVBO());
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), (void*)0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), (void*)sizeof(Vector2));
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}

	// unbind objects, and reset the viewport
	glBindVertexArray(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0); // bind back to default framebuffer
	glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

	// release objects
	if(fbo) glDeleteFramebuffers(1, &fbo);
	if(vao) glDeleteVertexArrays(1, &vao);
}

bool SpritePacker::Export(std::string filePath)
{
    bool success = false;
	int32_t width = 0;
	int32_t height = 0;
	unsigned char *pixels = nullptr;
    FILE *fp = nullptr;
    FIBITMAP *dib = nullptr;

	// get and write the texture's dimensions as long as they're valid
	glBindTexture(GL_TEXTURE_2D, atlasTexture);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
	if(!width || !height)
	{
		fprintf(stderr, "invalid texture dimensions<%i  %i>\n", width, height);
		return false;
    }

	// allocate, and retrieve the pixel data
	pixels = new unsigned char[width * height * 4];
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

    // allocate the FreeImage object, and fill it with pixel data
    dib = FreeImage_ConvertFromRawBits(pixels, width, height, width * 4, 32, 0xFF0000, 0x00FF00, 0x0000FF, false);
    SwapRedBlue32(dib);

    // save the image to file, then release FreeImage object and pixel buffer
    success = FreeImage_Save(FIF_PNG, dib, filePath.c_str(), PNG_DEFAULT);
    FreeImage_Unload(dib);
    delete [] pixels;

    // make sure FreeImage succeeded in saving the image to file
    if(!success)
    {
        fprintf(stderr, "unable to save image: %s", filePath.c_str());
        return false;
    }

	// attempt to open the file
	filePath += ".json";
    fp = fopen(filePath.c_str(), "w");
	if(!fp)
	{
		fprintf(stderr, "unable to write to path: %s\n", filePath.c_str());
		return false;
	}

	// write all rect data
	std::string json = "[\n";
	for(int i=0;i<GetNumRects();++i)
	{
		// only output the rect if it can be packed
		if(rects[i]->CanPack())
		{
			char text[512];
			char delimeter = (i == GetNumRects() - 1) ? ' ' : ',';
			std::string rotatedBoolean = rects[i]->IsRotated() ? "true" : "false";
			sprintf(text, "\t{ \"name\" : \"%s\", \"rotated\" : %s, \"rect\" : [%i, %i, %i, %i] }%c\n",
					rects[i]->GetFilename().c_str(), rotatedBoolean.c_str(),
					(int)rects[i]->position.x, (int)rects[i]->position.y,
					(int)rects[i]->size.x, (int)rects[i]->size.x, delimeter);
			json += text;
		}
	}
	json += "]";

	// write the string to the file, and close it
	fwrite(json.c_str(), 1, json.length(), fp);
	fclose(fp);
	return true;
}
