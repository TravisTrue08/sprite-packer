#ifndef _SPRITE_PACKER_H_
#define _SPRITE_PACKER_H_

#include "BinPacker.h"
#include "GLHelper.h"

#include <string>


class SpriteBinRect : public BinRect
{
	friend class SpritePacker;

protected:
	GLuint texture;
	GLuint vbo;
	std::string filePath;

	SpriteBinRect(std::string filePath, GLint width, GLint height, GLenum imageFormat, GLenum type, void *data);
	void GenerateVertices();

public:
	static SpriteBinRect *Create(std::string filePath);
    virtual ~SpriteBinRect();
	
	bool CanPack() { return texture != 0 ? true : false; }
	GLuint GetTexture() { return texture; }
	GLuint GetVBO() { return vbo; }
	std::string GetFilePath() { return filePath; }
	std::string GetFilename() const { return filePath.substr(filePath.find_last_of("/") + 1); }
};


class SpritePacker : public BinPacker<SpriteBinRect>
{	
private:
	static bool initialized;
	static GLint orthoMatLoc;
	static GLint zoomLoc;
	static GLint textureLoc;
	static GLuint spriteProgram;

	GLuint atlasTexture;
	Color32f backgroundColor;

protected:
	void PostCommit();

public:
	SpritePacker();
	~SpritePacker();

	static void Initialize();
	static void Shutdown();
	static GLint GetOrthoMatLoc() { return orthoMatLoc; }
	static GLint GetZoomLoc() { return zoomLoc; }
	static GLint GetTextureLoc() { return textureLoc; }

	bool Export(std::string filePath);

	void SetSpread(int value);
	void SetBackgroundColor(Color32f color) { backgroundColor = color; }
	void SetBackgroundColor(float r, float g, float b, float a) { backgroundColor.Set(r, g, b, a); }
	GLuint GetSpriteProgram() { return spriteProgram; }
	GLuint GetAtlasTexture() { return atlasTexture; }
};

#endif
