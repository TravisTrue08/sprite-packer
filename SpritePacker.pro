#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T22:21:28
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

TARGET = SpritePacker
TEMPLATE = app
CONFIG += c++11


# preprocessor directives
win32:DEFINES	+= PLATFORM_WINDOWS _CRT_SECURE_NO_WARNINGS
macx:DEFINES	+= PLATFORM_OSX


# include path
INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include


# Windows libs
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/Windows -lFreeImage -lglew32
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/Windows -lFreeImaged -lglew32d


# OS X libs
macx: LIBS += -L$$PWD/lib/Mac -lFreeImage -lGLEW


SOURCES +=	main.cpp \
			MainWindow.cpp \
			BinPacker.cpp \
			GLHelper.cpp \
			GLWidget.cpp \
			Matrix.cpp \
			Types.cpp \
			Vector.cpp \
			SpritePacker.cpp \
			SpriteItemModel.cpp \
			SpriteListView.cpp \
			ZoomWidget.cpp \
			ColorWidget.cpp

HEADERS  +=	MainWindow.h \
			BinPacker.h \
			GLHelper.h \
			GLWidget.h \
			Matrix.h \
			Types.h \
			Vector.h \
			SpritePacker.h \
			SpriteItemModel.h \
			SpriteListView.h \
			ZoomWidget.h \
			ColorWidget.h

FORMS +=	MainWindow.ui \
			ZoomWidget.ui
