/***********************************************************************************
*
*		The MIT License
*
*		Copyright (c) 2013 Travis J True
*
*	Permission is hereby granted, free of charge, to any person obtaining a copy of
*	this software and associated documentation files (the "Software"), to deal in
*	the Software without restriction, including without limitation the rights to
*	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
*	the Software, and to permit persons to whom the Software is furnished to do so,
*	subject to the following conditions:
*
*	The above copyright notice and this permission notice shall be included in all
*	copies or substantial portions of the Software.
*
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
*	FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
*	COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
*	IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
*	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*
**********************************************************************************/

#include "Types.h"


Color32b Color32b::operator=(Color32b opt2)
{
	Set(opt2.red, opt2.green, opt2.blue, opt2.alpha);
	return *this;
}

Color32b Color32b::operator=(Color24f opt2)
{
	Set((uint8_t)opt2.red, (uint8_t)opt2.green, (uint8_t)opt2.blue, 255);
	return *this;
}

Color32b Color32b::operator=(Color32f opt2)
{
	Set((uint8_t)(opt2.red   * 255.0f), (uint8_t)(opt2.green * 255.0f), (uint8_t)(opt2.blue  * 255.0f), (uint8_t)(opt2.alpha * 255.0f));
	return *this;
}

void Color24f::Set(float red, float green, float blue)
{
	this->red = red;
	this->green = green;
	this->blue = blue;
}

Color24f Color24f::operator=(Color32b opt2)
{
	Set((float)opt2.red / 255.0f, (float)opt2.green / 255.0f, (float)opt2.blue / 255.0f);
	return *this;
}

Color24f Color24f::operator=(Color24f opt2)
{
	Set(opt2.red, opt2.green, opt2.blue);
	return *this;
}

Color24f Color24f::operator=(Color32f opt2)
{
	Set(opt2.red, opt2.green, opt2.blue);
	return *this;
}

void Color32f::Set(float red, float green, float blue, float alpha)
{
	this->red = red;
	this->green = green;
	this->blue = blue;
	this->alpha = alpha;
}

Color32f Color32f::operator=(Color32b opt2)
{
	red = (float)opt2.red / 255.0f;
	green = (float)opt2.green / 255.0f;
	blue = (float)opt2.blue / 255.0f;
	alpha = (float)opt2.alpha / 255.0f;
	return *this;
}

Color32f Color32f::operator=(Color24f opt2)
{
	red = opt2.red;
	green = opt2.green;
	blue = opt2.blue;
	alpha = 255;
	return *this;
}

Color32f Color32f::operator=(Color32f opt2)
{
	red = opt2.red;
	green = opt2.green;
	blue = opt2.blue;
	alpha = opt2.alpha;
	return *this;
}
