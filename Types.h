/***********************************************************************************
*
*		The MIT License
*
*		Copyright (c) 2013 Travis J True
*
*	Permission is hereby granted, free of charge, to any person obtaining a copy of
*	this software and associated documentation files (the "Software"), to deal in
*	the Software without restriction, including without limitation the rights to
*	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
*	the Software, and to permit persons to whom the Software is furnished to do so,
*	subject to the following conditions:
*
*	The above copyright notice and this permission notice shall be included in all
*	copies or substantial portions of the Software.
*
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
*	FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
*	COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
*	IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
*	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*
**********************************************************************************/

#ifndef _MATH_TYPES_H_
#define _MATH_TYPES_H_

#include "Vector.h"
#include <stdint.h>


struct Color32b;
struct Color24f;
struct Color32f;


struct Color24b
{
	uint8_t red, green, blue;

	Color24b() : red(255), green(255), blue(255) { }
	Color24b(uint8_t red, uint8_t green, uint8_t blue) : red(red), green(green), blue(blue) { }

	void Set(uint8_t red, uint8_t green, uint8_t blue)
	{
		this->red = red;
		this->green = green;
		this->blue = blue;
	}
};


struct Color32b
{
	uint8_t red, green, blue, alpha;

	Color32b() : red(255), green(255), blue(255), alpha(255) { }
	Color32b(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha) : red(red), green(green), blue(blue), alpha(alpha) { }

	Color32b operator=(Color32b opt2);
	Color32b operator=(Color24f opt2);
	Color32b operator=(Color32f opt2);

	void Set(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
	{
		this->red = red;
		this->green = green;
		this->blue = blue;
		this->alpha = alpha;
	}
};


struct Color24f
{
	float red, green, blue;

	Color24f() : red(1.0f), green(1.0f), blue(1.0f) { }
	Color24f(float red, float green, float blue) : red(red), green(green), blue(blue) { }

	void Set(float red, float green, float blue);

	Color24f operator=(Color32b opt2);
	Color24f operator=(Color24f opt2);
	Color24f operator=(Color32f opt2);
	Color24f operator+(Color24f opt2) { return Color24f(red + opt2.red, green + opt2.green, blue + opt2.blue); }
	Color24f operator+(float opt2) { return Color24f(red + opt2, green + opt2, blue + opt2); }
	Color24f operator-(Color24f opt2) { return Color24f(red - opt2.red, green - opt2.green, blue - opt2.blue); }
	Color24f operator-(float opt2) { return Color24f(red - opt2, green - opt2, blue - opt2); }
	Color24f operator*(Color24f opt2) { return Color24f(red * opt2.red, green * opt2.green, blue * opt2.blue); }
	Color24f operator*(float opt2) { return Color24f(red * opt2, green * opt2, blue * opt2); }
	Color24f operator/(Color24f opt2) { return Color24f(red / opt2.red, green / opt2.green, blue / opt2.blue); }
	Color24f operator/(float opt2) { return Color24f(red / opt2, green / opt2, blue / opt2); }
	bool operator==(Color24f opt2) { return (red == opt2.red && green == opt2.green && blue == opt2.blue) ? true : false; }
	bool operator!=(Color24f opt2) { return (red != opt2.red || green != opt2.green || blue != opt2.blue) ? true : false; }

	static Color24f InterpolateColor(Color24f startColor, Color24f endColor, float factor) { return startColor + (endColor - startColor) * factor; }
};


struct Color32f
{
	float red, green, blue, alpha;

	Color32f() : red(1.0f), green(1.0f), blue(1.0f), alpha(1.0f) { }
	Color32f(float red, float green, float blue, float alpha) : red(red), green(green), blue(blue), alpha(alpha) { }

	void Set(float red, float green, float blue, float alpha);

	Color32f operator=(Color32b opt2);
	Color32f operator=(Color24f opt2);
	Color32f operator=(Color32f opt2);
	Color32f operator+(Color32f opt2) { return Color32f(red + opt2.red, green + opt2.green, blue + opt2.blue, alpha + opt2.alpha); }
	Color32f operator+(float opt2) { return Color32f(red + opt2, green + opt2, blue + opt2, alpha + opt2); }
	Color32f operator-(Color32f opt2) { return Color32f(red - opt2.red, green - opt2.green, blue - opt2.blue, alpha - opt2.alpha); }
	Color32f operator-(float opt2) { return Color32f(red - opt2, green - opt2, blue - opt2, alpha - opt2); }
	Color32f operator*(Color32f opt2) { return Color32f(red * opt2.red, green * opt2.green, blue * opt2.blue, alpha * opt2.alpha); }
	Color32f operator*(float opt2) { return Color32f(red * opt2, green * opt2, blue * opt2, alpha * opt2); }
	Color32f operator/(Color32f opt2) { return Color32f(red / opt2.red, green / opt2.green, blue / opt2.blue, alpha / opt2.alpha); }
	Color32f operator/(float opt2) { return Color32f(red / opt2, green / opt2, blue / opt2, alpha / opt2); }
	bool operator==(Color32f opt2) { return (red == opt2.red && green == opt2.green && blue == opt2.blue && alpha == opt2.alpha) ? true : false; }
	bool operator!=(Color32f opt2) { return (red != opt2.red || green != opt2.green || blue != opt2.blue || alpha != opt2.alpha) ? true : false; }

	static Color32f InterpolateColor(Color32f startColor, Color32f endColor, float factor) { return startColor + (endColor - startColor) * factor; }
};


struct Rect
{
	Vector2 position;
	Vector2 size;

	Rect() { }
	Rect(float width, float height) : size(width, height) { }
	Rect(float x, float y, float width, float height) : position(x, y), size(width, height) { }

	bool operator>(const Rect &opt2) const { return GetArea() > opt2.GetArea(); }
	bool operator<(const Rect &opt2) const { return GetArea() < opt2.GetArea(); }
	bool operator>=(const Rect &opt2) const { return GetArea() >= opt2.GetArea(); }
	bool operator<=(const Rect &opt2) const { return GetArea() <= opt2.GetArea(); }
	bool operator==(const Rect &opt2) const { return GetArea() == opt2.GetArea(); }
	bool operator!=(const Rect &opt2) const { return GetArea() != opt2.GetArea(); }

	float GetArea() const { return size.x * size.y; }
};


class Plane
{
private:
	float distance;
	Vector3 normal;

public:
	Plane() : distance(0.0f) { }
	Plane(Vector3 point, Vector3 normal) { Compute(point, normal); }

	void Compute(Vector3 point, Vector3 normal)
	{
		this->normal = normal; // assume it's already normalized
		distance = -Vector3::Dot(point, normal);
	}

	float GetDistance()	{ return distance; }
	Vector3 GetNormal()	{ return normal; }
};

#endif
