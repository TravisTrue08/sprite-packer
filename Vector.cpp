#include "Vector.h"


Vector3 Vector3::right(1.0f, 0.0f, 0.0f);
Vector3 Vector3::up(0.0f, 1.0f, 0.0f);
Vector3 Vector3::forward(0.0f, 0.0f, 1.0f);


Vector2 Vector2::operator=(const Vector2 opt2)
{
	x = opt2.x;
	y = opt2.y;
	return *this;
}

Vector2 Vector2::operator=(const Vector3 opt2)
{
	x = opt2.x;
	y = opt2.y;
	return *this;
}

Vector2 Vector2::operator=(const Vector4 opt2)
{
	x = opt2.x;
	y = opt2.y;
	return *this;
}

Vector2 Vector2::operator+=(Vector2 opt2)
{
	x += opt2.x;
	y += opt2.y;
	return *this;
}

Vector2 Vector2::operator-=(Vector2 opt2)
{
	x -= opt2.x;
	y -= opt2.y;
	return *this;
}

Vector2 Vector2::operator*=(const float opt2)
{
	x *= opt2;
	y *= opt2;
	return *this;
}

Vector2 Vector2::operator/=(const float opt2)
{
	x /= opt2;
	y /= opt2;
	return *this;
}

float Vector2::Normalize()
{
	float magnitude = Magnitude();
	if(!magnitude) return magnitude;
	*this /= magnitude;
	return magnitude;
}

Vector3 Vector3::Cross(Vector3 a, Vector3 b)
{
	Vector3 temp;
	temp.x = (a.y * b.z) - (a.z * b.y);
	temp.y = (a.z * b.x) - (a.x * b.z);
	temp.z = (a.x * b.y) - (a.y * b.x);
	return temp;
}

Vector3 Vector3::operator=(Vector2 opt2)
{
	x = opt2.x;
	y = opt2.y;
	z = 0.0f;
	return *this;
}

Vector3 Vector3::operator=(Vector3 opt2)
{
	x = opt2.x;
	y = opt2.y;
	z = opt2.z;
	return *this;
}

Vector3 Vector3::operator=(Vector4 opt2)
{
	x = opt2.x;
	y = opt2.y;
	z = opt2.z;
	return *this;
}

Vector3 Vector3::operator+=(Vector3 opt2)
{
	x += opt2.x;
	y += opt2.y;
	z += opt2.z;
	return *this;
}

Vector3 Vector3::operator*=(float opt2)
{
	x *= opt2;
	y *= opt2;
	z *= opt2;
	return *this;
}

Vector3 Vector3::operator/=(float opt2)
{
	x /= opt2;
	y /= opt2;
	z /= opt2;
	return *this;
}

Vector3 Vector3::operator-=(Vector3 opt2)
{
	x -= opt2.x;
	y -= opt2.y;
	z -= opt2.z;
	return *this;
}

float Vector3::Normalize()
{
	float magnitude = Magnitude();
	if(!magnitude) return magnitude;
	*this /= magnitude;
	return magnitude;
}

Vector4 Vector4::operator=(const Vector2 opt2)
{
	x = opt2.x;
	y = opt2.y;
	z = 0.0f;
	w = 0.0f;
	return *this;
}

Vector4 Vector4::operator=(const Vector3 opt2)
{
	x = opt2.x;
	y = opt2.y;
	z = opt2.z;
	w = 0.0f;
	return *this;
}

Vector4 Vector4::operator=(const Vector4 opt2)
{
	x = opt2.x;
	y = opt2.y;
	z = opt2.z;
	w = opt2.w;
	return *this;
}

Vector4 Vector4::operator+=(Vector4 opt2)
{
	x += opt2.x;
	y += opt2.y;
	z += opt2.z;
	w += opt2.w;
	return *this;
}

Vector4 Vector4::operator*=(float opt2)
{
	x *= opt2;
	y *= opt2;
	z *= opt2;
	w *= opt2;
	return *this;
}

Vector4 Vector4::operator/=(float opt2)
{
	x /= opt2;
	y /= opt2;
	z /= opt2;
	w /= opt2;
	return *this;
}

Vector4 Vector4::operator-=(Vector4 opt2)
{
	x -= opt2.x;
	y -= opt2.y;
	z -= opt2.z;
	w -= opt2.w;
	return *this;
}

float Vector4::Normalize()
{
	float magnitude = Magnitude();
	if (!magnitude) return magnitude;
	*this /= magnitude;
	return magnitude;
}
