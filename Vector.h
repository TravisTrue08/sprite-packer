#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <cmath>
#include <string>


class Vector2;
class Vector3;
class Vector4;
	
	
class Vector2
{
public:
	float x, y;

	static float Dot(const Vector2 a, const Vector2 b) { return (a.x * b.x) + (a.y * b.y); }
	static Vector2 Zero() { return Vector2(); }
	static Vector2 One() { return Vector2(1.0f, 1.0f); }
	
	// constructors/destructor
	Vector2() : x(0.0f), y(0.0f) { }
	Vector2(float x, float y) : x(x), y(y) { }
	
	// operator overloads
	Vector2 operator=(Vector2 opt2);
	Vector2 operator=(Vector3 opt2);
	Vector2 operator=(Vector4 opt2);
	Vector2 operator+(Vector2 opt2) { return Vector2(x + opt2.x, y + opt2.y); }
	Vector2 operator-(Vector2 opt2) { return Vector2(x - opt2.x, y - opt2.y); }
	Vector2 operator*(Vector2 opt2) { return Vector2(x * opt2.x, y * opt2.y); }
	Vector2 operator*(float opt2) { return Vector2(x * opt2, y * opt2); }
	Vector2 operator/(Vector2 opt2) { return Vector2(x / opt2.x, y / opt2.y); }
	Vector2 operator/(float opt2) { return Vector2(x / opt2, y / opt2); }
	Vector2 operator+=(Vector2 opt2);
	Vector2 operator-=(Vector2 opt2);
	Vector2 operator*=(float opt2);
	Vector2 operator/=(float opt2);
	bool operator==(const Vector2 &opt2) const { return (x == opt2.x && y == opt2.y) ? true : false; }
	bool operator!=(const Vector2 &opt2) const { return (x != opt2.x && y != opt2.y) ? true : false; }
	
	float MagnitudeSq()			{ return (x * x) + (y * y); }
	float Magnitude()			{ return sqrtf(MagnitudeSq()); }
	float Normalize();

	void Set(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
};
	
	
class Vector3
{
private:
	static Vector3 right, up, forward;
		
public:
	float x, y, z;
	
	static float Dot(const Vector3 a, const Vector3 b) { return (a.x * b.x) + (a.y * b.y) + (a.z * b.z); }
	static Vector3 Cross(Vector3 a, Vector3 b);
	static Vector3 GetRight()			{ return right;		}
	static Vector3 GetUp()				{ return up;		}
	static Vector3 GetForward()			{ return forward;	}
	static Vector3 Zero() { return Vector3(); }
	static Vector3 One() { return Vector3(1.0f, 1.0f, 1.0f); }
	
	// constructors/destructor
	Vector3() : x(0.0f), y(0.0f), z(0.0f) { }
	Vector3(float x, float y, float z) : x(x), y(y), z(z) { }
	
	// operator overloads
	Vector3 operator=(Vector2 opt2);
	Vector3 operator=(Vector3 opt2);
	Vector3 operator=(Vector4 opt2);
	Vector3 operator+(Vector3 opt2) { return Vector3(x + opt2.x, y + opt2.y, z + opt2.z); }
	Vector3 operator-(Vector3 opt2) { return Vector3(x - opt2.x, y - opt2.y, z - opt2.z); }
	Vector3 operator*(Vector3 opt2) { return Vector3(x * opt2.x, y * opt2.y, z * opt2.z); }
	Vector3 operator*(float opt2) { return Vector3(x * opt2, y * opt2, z * opt2); }
	Vector3 operator/(Vector3 opt2) { return Vector3(x / opt2.x, y / opt2.y, z / opt2.z); }
	Vector3 operator/(float opt2) { return Vector3(x / opt2, y / opt2, z / opt2); }
	Vector3 operator+=(Vector3 opt2);
	Vector3 operator-=(Vector3 opt2);
	Vector3 operator*=(float opt2);
	Vector3 operator/=(float opt2);
	bool    operator==(const Vector3 &opt2) const { return (x == opt2.x && y == opt2.y && z == opt2.z) ? true : false; }
	bool    operator!=(const Vector3 &opt2) const { return (x != opt2.x || y != opt2.y || z != opt2.z) ? true : false; }
	
	float MagnitudeSq() { return (x * x) + (y * y) + (z * z); }
	float Magnitude() { return sqrtf(MagnitudeSq()); }
	float Normalize();

	void Set(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
};
	
	
class Vector4
{
public:
	float x, y, z, w;
	
	static float Dot(const Vector4 a, const Vector4 b) { return (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w); }
	static Vector4 Zero() { return Vector4(); }
	static Vector4 One() { return Vector4(1.0f, 1.0f, 1.0f, 1.0f); }
	
	// constructors/destructor
	Vector4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
	Vector4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }

	// operator overloads
	Vector4 operator=(Vector2 opt2);
	Vector4 operator=(Vector3 opt2);
	Vector4 operator=(Vector4 opt2);
	Vector4 operator+(Vector4 opt2) { return Vector4(x + opt2.x, y + opt2.y, z + opt2.z, w + opt2.w); }
	Vector4 operator-(Vector4 opt2) { return Vector4(x - opt2.x, y - opt2.y, z - opt2.z, w - opt2.w); }
	Vector4 operator*(Vector4 opt2) { return Vector4(x * opt2.x, y * opt2.y, z * opt2.z, w * opt2.w); }
	Vector4 operator*(float opt2) { return Vector4(x * opt2, y * opt2, z * opt2, w * opt2); }
	Vector4 operator/(Vector4 opt2) { return Vector4(x / opt2.x, y / opt2.y, z / opt2.z, w / opt2.w); }
	Vector4 operator/(float opt2) { return Vector4(x / opt2, y / opt2, z / opt2, w / opt2); }
	Vector4 operator+=(Vector4 opt2);
	Vector4 operator-=(Vector4 opt2);
	Vector4 operator*=(float opt2);
	Vector4 operator/=(float opt2);
	bool    operator==(const Vector4 &opt2) const { return (x == opt2.x && y == opt2.y && z == opt2.z && w == opt2.w) ? true : false; }
	bool    operator!=(const Vector4 &opt2) const { return (x != opt2.x || y != opt2.y || z != opt2.z || w != opt2.w) ? true : false; }
	
	float MagnitudeSq()			{ return (x * x) + (y * y) + (z * z) + (w * w); }
	float Magnitude()			{ return sqrtf(MagnitudeSq()); }
	float Normalize();

	void Set(float x, float y, float z, float w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
};

#endif
