﻿#include "ZoomWidget.h"
#include "ui_ZoomWidget.h"


ZoomWidget::ZoomWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::ZoomWidget),
	zoom(1.0f)
{
	ui->setupUi(this);
}

ZoomWidget::~ZoomWidget()
{
	delete ui;
}

void ZoomWidget::on_zoomSpinner_valueChanged(double value)
{
	ui->zoomSlider->setValue((int)(value * 100.0));
	SetZoom(value);
}

void ZoomWidget::on_zoomSlider_valueChanged(int value)
{
	ui->zoomSpinner->setValue((double)value / 100.0);
	SetZoom((float)value / 100.0f);
}

void ZoomWidget::on_magnifyButton_clicked()
{
	// iterate forward through all pre-determined ZoomFactors until the second-to-last index
	for(int i=0;i<NumZoomFactors;++i)
	{
		// check if the current factor is greater than the current zoom
		if(ZoomFactors[i] > GetZoom())
		{
			// update the zoom to that factor, update the UI and finish
			SetZoom(ZoomFactors[i]);
			UpdateUI();
			break;
		}
	}
}

void ZoomWidget::on_minifyButton_clicked()
{
	// iterate backward through all pre-determined ZoomFactors until the second index
	for(int i=NumZoomFactors-1;i>=0;--i)
	{
		// check if the current factor is less than the current zoom
		if(ZoomFactors[i] < GetZoom())
		{
			// update the zoom to that factor, update the UI and finish
			SetZoom(ZoomFactors[i]);
			UpdateUI();
			break;
		}
	}
}

void ZoomWidget::UpdateUI()
{
	ui->zoomSlider->setValue((int)(GetZoom() * 100.0f));
	ui->zoomSpinner->setValue((double)GetZoom());
}

void ZoomWidget::IncrementZoom(float offset)
{
	SetZoom(zoom + offset);
	UpdateUI();
}

void ZoomWidget::SetZoom(float zoom)
{
	// bound the incoming zoom parameter's value
	if(zoom < GetMinZoom()) zoom = GetMinZoom();
	if(zoom > GetMaxZoom()) zoom = GetMaxZoom();

	// update the value and UI widgets (we keep a separate value for consistency)
	this->zoom = zoom;
	emit ZoomUpdated(zoom);
}
