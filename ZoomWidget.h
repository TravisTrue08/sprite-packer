﻿#ifndef _ZOOM_WIDGET_H_
#define _ZOOM_WIDGET_H_

#include <QWidget>

namespace Ui {
class ZoomWidget;
}


const int NumZoomFactors = 10;
const float ZoomFactors[NumZoomFactors] = {
	0.25f,
	0.5f,
	0.75f,
	1.0f,
	1.25f,
	1.5f,
	1.75f,
	2.0f,
	3.0f,
	4.0f
};


class ZoomWidget : public QWidget
{
	Q_OBJECT

private:
	Ui::ZoomWidget *ui;
	float zoom;

public:
	explicit ZoomWidget(QWidget *parent = 0);
	~ZoomWidget();

private slots:
	void on_zoomSpinner_valueChanged(double arg1);
	void on_zoomSlider_valueChanged(int value);
	void on_magnifyButton_clicked();
	void on_minifyButton_clicked();

public slots:
	void UpdateUI();
	void IncrementZoom(float offset);

public:
	void SetZoom(float zoom);

	float GetZoom() { return zoom; }
	float GetMinZoom() { return ZoomFactors[0]; }
	float GetMaxZoom() { return ZoomFactors[NumZoomFactors-1]; }

signals:
	void ZoomUpdated(float zoom);
};

#endif // _ZOOM_WIDGET_H_
